/*
 * Copyright 2015 Laszlo Kis-Adam <laszlo.kis-adam@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "configpage.h"
#include "ui_configpage.h"
#include <interfaces/iproject.h>

#include <KSharedConfig>
#include <KConfigGroup>

using namespace KDevelop;

namespace JSHint
{

ConfigPage::ConfigPage(KDevelop::IProject *project, QWidget *parent)
    : KDevelop::ConfigPage(nullptr, nullptr, parent)
    , m_ui(new Ui::JSHintConfigPage())
    , m_project(project)
{
    m_ui->setupUi(this);
}

ConfigPage::~ConfigPage()
{
}

QString ConfigPage::name() const
{
    return QStringLiteral("JSHint");
}

void ConfigPage::apply()
{
    KConfigGroup group = m_project->projectConfiguration()->group("JSHint");

    group.writeEntry("AdditionalArguments", m_ui->additionalArguments->text());
    group.writeEntry("Extensions", m_ui->extensions->text());
}

void ConfigPage::defaults()
{
    reset();
}

void ConfigPage::reset()
{    
    KConfigGroup group = m_project->projectConfiguration()->group("JSHint");

    m_ui->additionalArguments->setText(group.readEntry("AdditionalArguments", QString()));
    m_ui->extensions->setText(group.readEntry("Extensions", QStringLiteral(".js")));
}

}

