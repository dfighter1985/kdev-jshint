/*
 * Copyright 2015 Laszlo Kis-Adam <laszlo.kis-adam@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <QTest>
#include "../parser.h"

#include <kdevplatform/tests/testcore.h>
#include <kdevplatform/tests/autotestshell.h>

using namespace KDevelop;
using namespace JSHint;

namespace
{

const QString fileName = QStringLiteral("examples/test.js");

struct ProblemData
{
    int line;
    int column;
    QString msg;
};


ProblemData testData[] = {
    { 12, 1, QStringLiteral("This is just a hint") },
    { 34, 2, QStringLiteral("This is a 'stern warning'!") },
    { 56, 3, QStringLiteral("This is something, something 'dark side'.") }
};

/// Generates an input line from the test data
QString makeLine(const ProblemData &data)
{
    QString line;

    line += fileName;
    line += ':';
    line += " line ";
    line += QString::number(data.line);
    line += ", col ";
    line += QString::number(data.column);
    line += ", ";
    line += data.msg;
    line += '\n';
    return line;
}

}

class TestParser : public QObject
{
    Q_OBJECT
private slots:
    void initTestCase();
    void cleanupTestCase();

    void testParser();

private:
    QString generateInput();

    QScopedPointer<Parser> m_parser;
};

void TestParser::initTestCase()
{
    AutoTestShell::init();
    TestCore::initialize(Core::NoUi);

    m_parser.reset(new Parser());
}

void TestParser::cleanupTestCase()
{
    TestCore::shutdown();
}

void TestParser::testParser()
{
    QString input = generateInput();

    m_parser->setInput(input);
    m_parser->parse();

    QVector<IProblem::Ptr> problems = m_parser->problems();

    /// Check if we caught all the issues
    int c = sizeof(testData) / sizeof(ProblemData);
    QCOMPARE(problems.count(), c);

    /// Compare them to the original
    for (int i = 0; i < problems.count(); i++) {
        QCOMPARE(problems[i]->description(), testData[i].msg);
        QCOMPARE(problems[i]->finalLocation().start().line(), testData[i].line - 1);
        QCOMPARE(problems[i]->finalLocation().start().column(), testData[i].column - 1);
        QCOMPARE(problems[i]->finalLocation().document.str(), fileName);
    }
}

QString TestParser::generateInput()
{
    QString input;

    input += makeLine(testData[0]);
    input += makeLine(testData[1]);
    input += makeLine(testData[2]);

    return input;
}

QTEST_MAIN(TestParser)

#include "test_parser.moc"
