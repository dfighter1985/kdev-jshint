/*
 * Copyright 2015 Laszlo Kis-Adam <laszlo.kis-adam@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "parser.h"
#include <QRegExp>

using namespace KDevelop;

namespace JSHint
{

Parser::Parser()
{
}

Parser::~Parser()
{
}

void Parser::setInput(const QString &in)
{
    /// Break up the input into lines
    m_lines = in.split('\n');
}

void Parser::parse()
{
    while (m_lines.size() > 0) {
        parseLine(m_lines.front());
        m_lines.pop_front();
    }
}

void Parser::parseLine(const QString &line)
{
    /// Check if the line contains a message
    /// /home/dfighter/build/fulljslint.js: line 730, col 33, 'hasOwnProperty' is a really bad name.
    QRegExp regExp("^[/.a-zA-Z0-9]*: line [0-9]*, col [0-9]*, ");
    int idx = line.indexOf(regExp);
    if (idx == -1)
        return;

    int colonIdx = line.indexOf(':');
    int firstCommaIdx = line.indexOf(',');
    int secondCommaIdx = line.indexOf(',', firstCommaIdx + 1);

    /// "/some/path/to/a/file"
    QString filePath = line.mid(0, colonIdx);
    /// "line 9001"
    QString lineMsg = line.mid(colonIdx + 2, firstCommaIdx - colonIdx - 2);
    /// "col 666"
    QString colMsg = line.mid(firstCommaIdx + 2, secondCommaIdx - firstCommaIdx - 2);
    /// "Some interesting message"
    QString message = line.mid(secondCommaIdx + 2);

    /// Cut the number and parse it into an int
    int lineNumber = lineMsg.mid(lineMsg.lastIndexOf(' ') + 1).toInt();
    int columnNumber  = colMsg.mid(colMsg.lastIndexOf(' ') + 1).toInt();

    IProblem::Ptr problem(new DetectedProblem());

    DocumentRange range;
    range.document = IndexedString(filePath);
    range.setBothLines(lineNumber - 1);
    range.setBothColumns(columnNumber - 1);

    problem->setFinalLocation(range);
    problem->setDescription(message);
    problem->setExplanation(message);

    problem->setSeverity(IProblem::Error);
    problem->setSource(IProblem::Plugin);

    m_problems.push_back(problem);
}

}

