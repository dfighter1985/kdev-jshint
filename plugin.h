/*
 * Copyright 2015 Laszlo Kis-Adam <laszlo.kis-adam@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


#ifndef KDEVJSHINT_PLUGIN_H
#define KDEVJSHINT_PLUGIN_H

#include <QVariant>
#include <interfaces/iplugin.h>
#include <interfaces/contextmenuextension.h>
#include <interfaces/iproblem.h>

class KJob;
class QAction;

namespace KDevelop
{
class ProblemModel;
class CheckerStatus;
}

namespace JSHint
{

/**
 * @brief JSHint plugin for KDevelop.
 *
 * It starts one job per file and sends the problems to the problems toolview.
 */
class Plugin : public KDevelop::IPlugin
{
    Q_OBJECT

public:
    Plugin(QObject *parent, const QVariantList & = QVariantList());
    virtual ~Plugin();
    virtual void unload();

    virtual KDevelop::ContextMenuExtension contextMenuExtension(KDevelop::Context* context) override;

    int configPages() const override { return 1; }
    KDevelop::ConfigPage* configPage(int number, QWidget *parent) override;

    int perProjectConfigPages() const override { return 1; }
    KDevelop::ConfigPage* perProjectConfigPage(int number, const KDevelop::ProjectConfigOptions &options, QWidget *parent);

private slots:
    /// Check the current file
    void checkFile();

    /// Check all project files
    void checkAllFiles();

    /// Triggered when the JSHint job finishes
    void onJobFinished(KJob *job);

private:
    /// Gather the parameters and start the check
    void check(bool allFiles);

    /// Check next file
    void checkNext();

    /// Creates and adds the JSHint actions to the Run menu
    void createActions();

    /// Creates and connects a JSHint file action
    QAction* createCheckFileAction();

    /// Creates and connects a JSHint all files action
    QAction* createCheckAllFilesAction();

    /// The problem model
    QScopedPointer<KDevelop::ProblemModel> m_model;

    /// Files queued for checking
    QStringList m_files;

    QVector<KDevelop::IProblem::Ptr> m_problems;
    QScopedPointer<KDevelop::CheckerStatus> m_status;
};


}

#endif


