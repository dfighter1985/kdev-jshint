What is this document?
======================
This document contains technical documentation about KDev-JSHint, a plugin for KDevelop.

High-level overview
===================
The plugin starts a background job for each selected files. The job checks the file(s) with JSHint for errors, and prints error 
messages if there are errors. The plugin parses this output and creates a KDevelop::DetectedProblem instance for each of those 
errors, which then get added to the JSHint tab in the Problems toolview.

JSHint analysis output
======================
JSHint prints an error message line for each issue found in the following format:

/home/dfighter/projects/test/s.js: line 11, col 11, 'a' is already defined.

The message contains the following information
* filename
* line where the issue was found
* column where the issue was found
* error message

The error lines can be matched with the following regular expression:

"^[/.a-zA-Z0-9]*: line [0-9]*, col [0-9]*, "

NOTE: The quotes are NOT part of the expression.

Classes
=======
See classes.jpg for class diagram.

Plugin
-------
Implements the KDevelop::IPlugin interface, and it handles the interactions with KDevelop. It creates
the config pages, handles user triggered events (e.g.: starting an analysis session), and initates and
handles the result of analysis sessions.
In the constructor is creates the Run menu actions and creates a ProblemModel instance, for storing the
problems found by JSHint.
When the user initiates an analysis it uses the active document's filename, or finds the project for the
active document, then uses the filenames found in the project, filtered by file extensions that can be set
in the per project config page. These files are then added to a queue, and check jobs are created one
after the other finishes.

Job
---
Implements the KJob interface. On construction it gathers the data needed for the analysis session. It
gets the executable path from the global "JSHint" group, gathers additional arguments from the
per-project "JSHint". On start it constructs the arguments from the gathered data for a QProcess instance, which then
executes the executable with those arguments. On sucess and failure too a singal is triggered.
The result of the analysis is passed to the parser.

Parser
------
Parses the result. It first breaks up the input to lines then matches those lines which are error messages,
then breaks them up to chunks and constructs KDevelop::DetectedProblem instances from the chunks.

ConfigPage
----------
Implements the KDevelop::ConfigPage interface. Provides a per-project config page stores the values
in the per-project "JSHint" group.

Key: AdditionalArguments
Explanation: Additional arguments passed to the executable, separated by space
Example: --verbose
Default: Empty by default

Key: Extensions
Explanation: Extensions of the files that will be checked, separated by semicolon.
Example: .abc;.def;.xyz
Default: .js

PreferencesPage
---------------
Implements the KDevelop::ConfigPage interface. Provides a global config page. Stores the values in
the global "JSHint" group.

Key: JSHint Path
Explanation: The full path to the JSHint executable
Example: /usr/local/bin/jshint
Default: /usr/local/bin/jshint


Collaboration of classes
========================
See sequence.jpg

